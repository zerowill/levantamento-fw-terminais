const robots = {
  input: require('./src/input.js'),
  state: require('./src/state.js'),
  getFWTerminais: require('./src/getFWTerminais.js'),
  getTerminaisCidades: require('./src/getTerminaisCidades.js')
}

async function start() {
  //robots.input()
  await robots.getFWTerminais()
  await robots.getTerminaisCidades()
}

start()