require('dotenv/config')
const puppeteer = require('puppeteer')
const fs = require('fs')

function getTerminaisCidades(){
  const acessaDatacenter = async () => {
    console.log(`Criando o navegador...`)
    const browser = await puppeteer.launch({
      headless: true,
      ignoreSSL: true,
      ignoreHTTPSErrors: true,
      args: [
        '--start-maximized',
        '--no-sandbox'
      ]
    })
    function dataFormatada(){
      const data = new Date(),
        dia  = data.getDate().toString()
        diaF = (dia.length == 1) ? '0'+dia : dia
        mes  = (data.getMonth()+1).toString() //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0'+mes : mes
        anoF = data.getFullYear()
      return `${anoF}-${mesF}-${diaF}`
    }
    console.log(`Iniciando o Chromium...`)
    /* JSON com dados das cidades */
    const rawdata = fs.readFileSync('dados-cidades.json')
    const dados = JSON.parse(rawdata)
    for (index = 0; index < dados.data.cod_cidade.length; index++){
      /* Abre o browser e acessa o site */
      console.log(`Levantando os dados da cidade de ${dados.data.nome_cidade[index]} em ${dataFormatada()}...`)
      const page = await browser.newPage()
      await page.setViewport({ width: 1366, height: 768, deviceScaleFactor: 1 })
      await page.authenticate({username:`${process.env.LOGIN}`, password:`${process.env.PASSWORD}`})
      console.log(`Acessando o site Datacenter...`)
      await page.goto(`http://datacenter.virtua.com.br/inventario/resumo_cidade.php?cod_cidade=${dados.data.cod_cidade[index]}&nome_cidade=${dados.data.nome_cidade[index]}&data=${dataFormatada()}%2017:00:00`)
      await page.waitForTimeout(500)
      const data = await page.evaluate(() => {
        const rows = Array.from(document.querySelectorAll('table[id="tabela"] > tbody > tr '))
        //return table.map(td => td.innerText)
        return Array.from(rows, row => {
          const columns = row.querySelectorAll('td')
          return Array.from(columns, column => column.innerText)
        })
      })
      /* Formata os dados em CSV */
      let csv = ""
      for (let index = 0; index < data.length; index++) {
        if(index != '1'){
          csv += data[index][0] + `;`
          csv += data[index][1] + `;`
          csv += data[index][2] + `;`
          csv += data[index][3] + `;`
          csv += data[index][4] + `;`
          csv += data[index][5] + `\n`
        }
      }
      console.log(`Finalizando a consulta de ${dados.data.nome_cidade[index]}...`)
      //console.log('@success', csv)
      fs.writeFileSync(`./Terminais_${dados.data.cod_cidade[index]}_${dataFormatada()}.csv`, csv)
    }
    /* Fecha o navegador */
    console.log(`Encerrando o Chromium...`)
    await browser.close()
    console.log(`Finalizando a consulta...`)
  }
  acessaDatacenter()
  .then((value) => {
  })
  .catch((error) => console.log(error))}

module.exports = getTerminaisCidades
