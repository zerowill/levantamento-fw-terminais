require('dotenv/config')
const fs = require('fs')
const puppeteer = require('puppeteer')

function getFWTerminais(){
  const acessaDatacenter = async () => {
    console.log(`Criando o navegador...`)
    const browser = await puppeteer.launch({
      headless: true,
      ignoreSSL: true,
      ignoreHTTPSErrors: true,
      args: [
        '--start-maximized',
        '--no-sandbox'
      ]
    })
    console.log(`Iniciando o Chromium...`)
    /* Abre o browser e acessa o site */
    console.log(`Levantando os dados das versões atualizadas...`)
    const page = await browser.newPage()
    await page.setViewport({ width: 1366, height: 768, deviceScaleFactor: 1 })
    await page.authenticate({username:`${process.env.LOGIN}`, password:`${process.env.PASSWORD}`})
    console.log(`Acessando o site Datacenter...`)
    await page.goto(`http://datacenter.virtua.com.br/inventario/resumo_por_modelo.php`)
    await page.waitForTimeout(1000)
    const data = await page.evaluate(() => {
      const rows = Array.from(document.querySelectorAll('table[id="tabela"] > tbody > tr '))
      return Array.from(rows, row => {
        const columns = row.querySelectorAll('td')
        return Array.from(columns, column => column.innerText)
      })
    })
    let csv = ""
    for (let index = 0; index < data.length; index++) {
      if(index != '1'){
        csv += data[index][0] + `;`
        csv += data[index][1] + `;`
        csv += data[index][2] + `\n`
      }
    }
    fs.writeFileSync(`./Updated FW Version.csv`, csv)
    /* Fecha o navegador */
    console.log(`Encerrando o Chromium...`)
    await browser.close()
    console.log(`Finalizando a consulta...`)
  }
  acessaDatacenter()
  .then((value) => {
  })
  .catch((error) => console.log(error))
}

module.exports = getFWTerminais
